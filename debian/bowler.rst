======
bowler
======

------------------------------------------------
Safe code refactoring for modern Python projects
------------------------------------------------

:Author: Facebook, Inc (edited & formatted by Nicholas D Steeves)
:Date: 2020-12-19
:Version: 0.9.0
:Manual section: 1


Synopsis
========

bowler [*OPTIONS*] *command* [--help] [*ARGUMENTS*]...


Description
===========

**bowler** is the command line interface to *Bowler*--a tool for
manipulating Python at the syntax tree level, thus enabling safe
large scale code modifications.  It is designed to make complicated
changes (such as function signature changes) simple, while ensuring
that the resulting code compiles and runs.  Bowler provides a simple
command line interface and a fluent API in Python for generating
complex code modifications.

Bowler uses a "fluent" `Query` API to build refactoring scripts
through a series of selectors, filters, and modifiers.  Many simple
modifications are already possible using the existing API, but one may
also provide custom selectors, filters, and modifiers to build more
complex refactorings.


Options
=======

--debug | --quiet

 Control the level of logging output from Bowler.  By default, Bowler
will only output warnings and errors.  With `--debug`, Bowler will also
output debug and info level messages.  With `--quiet`, Bowler will
run silently and will produce output only if an error is encountered.

--help

 Display all options and commands, or when given after a command, show
all options and arguments for that command.

-V, --version

 Print version string and exit.

Commands
========

**do** [--help] [-i, --interactive] [QUERY] [FILE]...

 Compile and run the given Python query, or open an IPython shell in
the absence of a query.  In either case, common Bowler API elements
are made available in the global namespace.

**dump** [--help] [--selector-pattern *PATTERN*] [FILE]...

 Load and dump the concrete syntax tree from the given FILE[s] to
standard output.

**run** [--help] [*FILE* | *MODULE*] [-- *ARGUMENTS*]...

 Execute a file-based code modification operation.

 Takes either the path to a Python script, or the name of an importable
module, and attempts to import it and, if found, run a "main()"
function from that script/module.  Extra arguments to this command
will be supplied to the script/module, and ARGUMENTS following "--"
will be forcibly passed through to it.

**test** [--help] [CODEMOD]

 Run tests defined in the codemod file.
